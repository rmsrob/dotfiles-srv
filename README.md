![bash](https://helloacm.com/wp-content/uploads/2016/03/bash-shellshock.png)

# Dotfiles for online instances

> Use this dotfiles on your instances

## Highlights

- bash only no zsh
- use vim over vi

## Install

```sh
mkdir ~/.dotfile
cd ~/.dotfiles
## with git
git clone https://gitlab.com/rmsrob/dotfiles-srv.git
## with wget
wget https://gitlab.com/rmsrob/dotfiles.git
```

## Usage

> you want to update your present config of bash or vim

```sh
ln -nsf ~/.dotfile/bash/bashrc.d ~/.bashrc.d
ln -nsf ~/.dotfile/bash/bash-scripts ~/.local/bin/bash-scripts
ln -nsf ~/.dotfile/bash/bashrc ~/.bashrc
ln -nsf ~/.dotfile/bash/bash-aliases ~/.bash-aliases
ln -nsf ~/.dotfile/bash/profile ~/.profile
ln -nsf ~/.dotfile/bash/inputrc ~/.inputrc
ln -nsf ~/.dotfile/bash/dircolors ~/.dircolors
ln -nsf ~/.dotfile/vim/vim.vim ~/.vimrc
mkdir ~/.vim
ln -nsf ~/.dotfile/vim/themes ~/.vim/.themes
ln -nsf ~/.dotfile/vim/settings ~/.vim/settings
ln -nsf ~/.dotfile/vim/plug ~/.vim/plug
```

## Maintainers

- [rmsrob][me]

[me]: https://gitlab.com/rmsrob
